---
title: Getting involved in the UNC App Lab
stem: get-involved
---

Students interested in learning how to build web and mobile apps are welcome to
participate in the App Lab at Sitterson Hall, room 027. You can [view the full
schedule](/calendar/) to see when the lab will be open. We're a friendly bunch,
so please drop in and say hi.

_Update for spring, 2022:_ also check our our [App-a-thon
competition](../app-a-thon/)!

### Projects

You are welcome to come to the lab with an app, whether it's just an idea or
you're currently developing it. If you have an idea for an app and want to find
other people to work on it with you, come by the App Lab and tell us about it.
We can help you with it, and we can also post it to [a Trello board where we
keep project ideas](https://trello.com/b/adrdTBg7/app-lab-ideas-board).

[The Trello board](https://trello.com/b/adrdTBg7/app-lab-ideas-board) also has
information about active projects, which you are welcome to join. This semester
(spring 2021), we have 3 active projects:

- A web app programmed in Clojure called Clem: the Clojure Error Mediator. It's
  intended to help make Clojure error messages easier to understand. ([Project
  board](https://trello.com/b/y3bceH3w/clem-the-clojure-error-mediator))
- A 2D Platformer for PC called Rhythme that's currently being developed using 
  Unity and C#. ([Project board](https://trello.com/b/2uARQEv5/rhythme))
- A web app that pulls information from a One Card scanner to create a check-in
  system for people to use when they get to the App Lab. ([Project board](https://trello.com/b/JLs248CT/check-in))

Again, you can see other ideas on [the project ideas Trello
board](https://trello.com/b/adrdTBg7/app-lab-ideas-board), which is publicly
visible (and come talk to us if you want to add an idea of your own).

### Communication tools

If you want to get involved, there are several tools that we use for
collaboration on projects.

- Slack for real-time chat, which you can [sign up for here](https://join.slack.com/t/unc-app-lab/signup).
- Trello for project management ([email us](mailto:applab@unc.edu) for access).
- A occasional newsletter, which you can [sign up for
  here](/newsletter-sign-up/).

### Skills you can learn

If you participate in the App Lab, here are some of the things you can expect to
learn:

- HTML (the language of web pages)
- CSS (how web pages are styled)
- data modeling (deciding what form information should take)
- storing data to and retrieving data from databases
- building web pages using programs (why and how)
- making data available to web pages with an API
- the difference between the 'frontend' and the 'backend'
- making web pages dynamic with in-browser programming
- how to build mobile applications
- patterns of application architecture
- functional programming techniques
- the programming languages Clojure, Clojurescript, and Javascript
- git for source code management
- software engineering tools and techniques, including project management boards,
  merge requests/pull requests, and code reviews
- graphic design of web pages

Because the App Lab isn't a course, you will need to take responsibility for
your own learning. If you show up, we will do our best to invite you to
participate in what we're working on and ask if you'd like help with an app of
your own. But you should also feel empowered to take the initiative and ask to
observe people working on their apps. If you have programming skills, you are
also encouraged to offer to pair program with them.

### Volunteering

You can get involved as a volunteer in the App Lab. Maybe you want to help your
chances of being a paid staff member next years. Or maybe you have some
particular skills that aren't well represented in the App Lab.

We ask volunteers to commit to 1 or more hours in the App Lab each week. We'll
post on [the hours page](/calendar/) who is volunteering when. To become a
volunteer:

1. Hang out in the App Lab and let us get to know you a bit.
2. [Email us](mailto:applab@unc.edu) that you're inerested in becoming a
   volunteer. Include what hours you'd like to be available. Also include your
   specialties, if any.
3. We will contact you to find a time to meet. We want to see your skills in
   action! And to make sure you know some important details about how the App
   Lab works.

### Learning Resources

If you're looking to learn new skills but don't know where to start, check out
the [resources page](https://applab.unc.edu/resources/)! There you'll find
lectures, videos, and websites to get you starting on everything from html to
backend programming!
