---
title: UNC App-a-thon (spring 2022)
stem: app-a-thon
---

The App-a-thon is a semester long programming competition where you can compete
by yourself or with a team to create the best possible app. The App Lab will
provide guidance and support, and the competition includes cash prizes!

Each team will participate in a specific track, and every team will be competing
for the secondary prizes.

<p style="text-align: center;">
  <img src="/App-A-thon-banner.jpg" alt="A flyer welcoming you to the app a thon"
  />
</p>

## Results!

Our first App-a-thon is over and we're so excited to announce our winners!

### Track Winners
- Game Track: Bao-Nhi Vu with [Avoid the Enemies](https://baonhi.itch.io/avoid-the-enemies)
- Web Track: Morgan Roberts with [Chromatic World](https://chromatic-world.web.app/)
- Mobile Track: App Team Carolina with
  [Centible](https://www.youtube.com/watch?v=QCVv6W2qo5I). Sign up for the [open
  beta](https://forms.gle/ydg69734aJkLtHAN8) here!

### Secondary Prize Winners
- Best UI/Design: Morgan Roberts with [Chromatic World](https://chromatic-world.web.app/)
- Most Impressive Functionality: App Team Carolina with [Centible](https://www.youtube.com/watch?v=QCVv6W2qo5I)
- Best Social Good: Ian Washabaugh and Ria Chheda with [No Puff Planters](https://youtu.be/vjTiVGpJE0A)

Thanks to everyone who participated, we enjoyed seeing everyone's submissions
and we hope you'll continue working on projects in and with the App Lab!

<hr />

**The rest of this website was intended for the students participating in the
Spring 2022 App-a-thon but is left in place for reference**

## Signing Up

To sign up have one team member fill out [this google
form](https://docs.google.com/forms/d/e/1FAIpQLSf_K2e-3wc0GJHTX_fQRjUS1NDw8R1yN5KBBEGQAOFtFqPFdQ/viewform)
by Janurary 25th at 11:59 PM. If you need help finding teammates, join the [App
Lab Slack](https://join.slack.com/t/unc-app-lab/signup) and ask in the
`#app-a-thon` channel. You can also use the Slack for communicating with your
team, just create a private channel.

## Format

Each team will be building an app that fits within one of three tracks:

- Mobile (iOS and Android apps)
- Web
- Game (in partnernship with the Game Dev Club)

Additionally, all teams will be eligible to receive the following secondary
prizes:

- Best UI/Design
- Most Impressive Functionality
- Best Social Good

Each track winner and secondary prize will include a cash prize, as well as
being featured at the closing ceremony.

Projects will be submitted through Github/Gitlab repositories, as well as some
form of deployment (the exact method depends on the track and each app).

## Prizes!

Each track includes a **$250** prize for the 1st place winning team, which will be
judged by the App Lab staff based on design, functionality, and viability. Each
secondary prize will also include a $250 prize.

## Workshops

Throughout the semester the App Lab will hold a variety of workshops in order to
support you in finishing your project. These will cover a variety of topics in
app development. The exact schedule is to be determined, but we will take your
interests and feedback into account. And of course you are welcome to come by
the App Lab for help even outside of a scheduled workshop.
 
## FAQ

- **Who can participate?**  
  The App-a-thon is open to all UNC students, undergraduate or graduate.
- **Can non-UNC students participate?**  
  All teams must have at least one UNC student, but may also have some non-UNC
  students
- **How big can teams be?**  
  Teams can be any size, but prizes are a fixed size, and larger teams can make
  it more difficult to complete a project. We recommend limiting teams to four members.

## Schedule

- Janurary 26th, 4:00-5:00 PM: Opening Ceremony and Projects Begin
- Workshops (all take place in the App Lab): 
  - UI/Design: Feb 9 at 2 PM 
  - React: Feb 16 at 2 PM
  - APIs: Feb 21 at 4 PM
  - iOS: Feb 28th at 5 PM
  - Firebase: March 8th at 3:30 PM 
- April 15th: Project Submission Deadline
- April 22nd: Awards Ceremony
