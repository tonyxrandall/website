---
title: Migrating HTML markup into code with Clojure and Hiccup
author: Jeff Terrell
date: 2018-09-29
tags: html, clojure, livecoding, video
---

In [the previous post](/posts/2018/09/28/implementing-graphic-design), I
implemented a graphic design as HTML and CSS. But the HTML will need to vary
depending on what's being displayed. We could simply duplicate the page for
every variation, but this is cumbersome and leads to brittleness. Instead, we
want to transition the HTML markup to a more malleable medium: code.

<!--more-->

Specifically, we'll be implementing the HTML as Clojure code that uses [the
Hiccup library](https://github.com/weavejester/hiccup) to produce equivalent
HTML markup. This will make future variation and extension of the markup easy.
In fact, in the video, I also implement a variation of the original page.

Here's [the video on YouTube](https://youtu.be/ceGtU5sMuPE) (68 minutes). Topics
include:

- adding ['middleware'](https://github.com/ring-clojure/ring/wiki/Concepts#middleware) to a a Clojure web server
- using feedback loops for development
- Clojure, including basic syntax, namespaces, and the Hiccup library
- HTML markup
