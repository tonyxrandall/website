---
title: Implementing a graphic design in HTML and CSS
author: Jeff Terrell
date: 2018-09-28
tags: html, css, design, livecoding, video
---

A common scenario professional web developers encounter is getting a graphic
design and needing to implement it as a web page in HTML and CSS. I encountered
this scenario with the Clem project, and I captured about 2 hours of video
capturing my (unrehearsed) attempt to solve it.

<!--more-->

If you don't know HTML and CSS, these videos might be a good way to get a feel
for what they are and how they're used. It's not a tutorial on these topics,
however, so you'll have to go elsewhere for a deeper dive. Here are some example
resources, none of which I've thoroughly vetted but which I'm acquainted with:

- Mozilla Developer's Network (MDN) -
  [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML) and
  [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- w3schools.com -
  [HTML](https://www.w3schools.com/html/default.asp) and
  [CSS](https://www.w3schools.com/css/default.asp)

If you _do_ know HTML and CSS to some extent, I expect that these videos will
help solidify the knowledge you have and give you some useful techniques for
setting up fast feedback loops and implementing a graphic design.

You can find the videos on YouTube:

- [Part 1](https://youtu.be/DfXAPMAeQXY) (60 minutes) - topics:
    - iterative refinement: starting with the big things and working on the details afterwards
    - using your browser's dev tools as a WYSIWYG CSS editor
    - CSS styles, including flexbox layout
    - HTML markup, including html5 tags
- [Part 2](https://youtu.be/4uFnbhB8mAM) (48 minutes) - topics:
    - using Google Fonts
    - using your browser's dev tools as a WYSIWYG CSS editor
    - CSS styles
    - HTML markup

There's also [a followup post](/posts/2018/09/29/migrating-html-hiccup/) (and
video) in which I convert the HTML document into Clojure code for use with
Hiccup.
