---
title: Building a Tic-Tac-Toe mobile app with React Native
author: Jeff Terrell
date: 2018-10-02
tags: react native, mobile, tutorial
---

[HackNC](https://hacknc.com/) happened the weekend of October 6&ndash;7th, 2018.
Leading up to the main event, there were a few talks, including one I gave on
React Native. It was intended for programmers who didn't have any prior
experience with React.js (on the web) or React Native (for mobile apps).

<!--more-->

Unfortunately, the talk didn't post to YouTube successfully, but you can check
out [the source
code](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe), which
includes notes on my step-by-step approach to building the app.
