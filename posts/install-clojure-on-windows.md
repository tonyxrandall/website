---
title: How to install Clojure on Windows
author: Jeff Terrell
date: 2019-09-11
tags: clojure, howto, windows
---

For reference, here is how we in the App Lab recommend installing Clojure. The
[official guide](https://clojure.org/guides/getting_started) works great for
MacOS and Linux users, but it currently has some confusing aspects for Windows
users that are worth clarifying.

<!--more-->

### Installing Java

First, check if you have Java installed already. Open a command prompt (e.g. by
typing `win+r cmd<enter>`) and type `java -version`. If you get reasonable
output, Java is already installed and you can move on to the next step.

If you don't have Java installed, download the Windows .zip version of [the
Java Development Kit 12](https://jdk.java.net/12/), extract it, and run the
installer. If you're successful, try the approach in the previous paragraph to
ensure Java is now installed and available. If that doesn't work, stop by the
App Lab for help.

### Installing Clojure with Leiningen

Download the Windows `lein.bat` script for [Leiningen](https://leiningen.org/).
You will probably have to right-click on the link and select 'Save link as...'
(or 'Save target as' in Microsoft Edge) in order to download it instead of open
it.

Open a command prompt (e.g. by typing `win+r cmd<enter>`), type `cd Downloads`
and hit enter to move to the directory with the downloaded .bat script.

Firefox users have an extra step here. Type `dir` and hit enter to see a list
of files in the current directory. If you see `lein.bat.txt` and not
`lein.bat`, you'll need to rename the file using this command: `rename
lein.bat.txt lein.bat`. Then check that the rename did what you expected by
saying `dir` again.

Now that you have `lein.bat` in the current directory, type `lein self-install`
and hit enter to install Leiningen. This shouldn't take more than a minute or two, depending on your network connection.

If you're successful, you should be able to type `lein repl` and hit enter to
get to a Clojure REPL (read-eval-print loop) and run Clojure code. If that
doesn't work, stop by the App Lab for help.

### Adding `lein` to your path (optional)

This step is optional, but if you don't do it, you will only be able to execute
the `lein` command when you have `cd`'d to your `Downloads` directory in your
command prompt (which you will need to do every time you open a new command
prompt window).

So, to make it possible to execute `lein` anywhere, we need to add the
directory containing the `lein.bat` file to the "path", so that the command
prompt will know where to find it. It's not ideal to have your Downloads
directory in your path, because then you might accidentally execute a
downloaded program instead of one that you intend. So we'll create a new
directory named `programs` in your home directory, move your `lein.bat` file
there, and add that directory to your path instead. To do that, issue these
commands in the command prompt:

```
cd ..
mkdir programs
move Downloads\lein.bat programs\
cd programs
```

Then, add the current directory to the path. First, use your mouse to select
the current directory, which is everything before the `>` on the bottom like of
your command prompt. My directory was `C:\Users\terrell\programs`, but you will
likely have a different user name than me. Then hit control-c to copy it to
your clipboard.

Finally, follow the directory in your clipboard to your path by following the
directions on [this
page](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/).
When you have to enter the directory, paste it with control-v&mdash;and
remember to add a trailing backslack (`\`) at the end.

To test that it worked, open a new command prompt window and type `lein repl`.
If it brings up a Clojure REPL, you're done. If it says that the `lein` command
could not be found, there was a problem. You can ask for help on [our Slack
group](https://join.slack.com/t/unc-app-lab/shared_invite/enQtNDMyMDQ4NjI1MTg1LTExYjJjNTgzNTA4MzFlMzk5NmU3ZWFmMWQyYTA0MjY5NWY0OGQ1OTlhMzk4NGM1ZDNkNmRhMGE1YTlhZDJjODA)
or swing by the App Lab (SN 027) for help.
