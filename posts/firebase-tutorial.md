---
title: Firebase Workshop
author: Daniel Manila
date: 2022-03-28
tags: workshop, tutorial, video, firebase
---

Firebase is a backend replacement produced by Google which helps developers
write fully featured web apps without having to handle things like databases and
authentication directly. This workshop deals with the basics of using firebase

<!-- more -->

## Overview

The workshop deals with the two most fundemental parts of Firebase, Firestore
and Authentication, using a simple todo app in pure javascript. By the end of
the tutorial, we'll have an app that automatically syncs across devices,
stores todo information, and only allows users to access their own todos.

## Links

- Recording: [Firebase Workshop](https://www.youtube.com/watch?v=trvXmCQ4-ms&feature=emb_title).
- Code: [Github
  repository](https://github.com/DonyorM/Firebase-Todo-example) to follow along. 
- To follow along in codepen: [Template](https://codepen.io/donyor/pen/abVrXYv)
- Slides: [Google slides deck](https://docs.google.com/presentation/d/1Yth-G6nOLxbqDyHnbY5mh1HtcS48UIrmmHb19L2_Pn4/edit?usp=sharing)
