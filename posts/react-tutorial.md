---
title: React Workshop
author: Nathaniel Fulmer
date: 2022-03-29
tags: workshop, tutorial, video, react
---

React is a free and open source Javascript library for building user interfaces based on UI components.

<!-- more -->

## Overview

This workshop introduces the fundamentals of React, including concepts like 
Hooks and Classes. Using online development tools like codepen, we quickly 
implement some basic functionality using React principles. We also get to 
see some real examples of React projects. By the end of the workshop, you
should have a thorough introduction into everything React. 

## Links

- Recording: [React Workshop](https://www.youtube.com/watch?v=4_BD6QZ8Y9c).
- To follow along in codepen: [Template](https://codepen.io/gaearon/pen/xEmzGg)
- Slides: [Google slides deck](https://docs.google.com/presentation/d/1_QGGDRVtrOHnCYo7KRF1b5V0RrwatFydjbyM_La0FsU/edit?usp=sharing)
