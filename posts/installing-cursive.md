---
title: Installing Cursive IDE for Clojure
author: Daniel Manila
date: 2019-09-27
tags: tutorial, clojure
---

[Cursive](https://cursive-ide.com) is an IDE built on top of IntelliJ that
supports syntax highlighting, code completion, repl integration and other
features for Clojure and ClojureScript. Unlike many other Clojure IDEs, it is
comparatively simple to install and has ordinary GUI.

<!--more-->

# Installation

1. Install [IntelliJ](https://www.jetbrains.com/idea/download/index.html) if you
   haven't already. The community edition is fine.
   - No need to import any settings
   - You can use default setting for UI Themes, Desktop Entry, and Launcher
     Script, if these steps in the setup exist.
   - For default plugins, if you are only using IntelliJ for Cursive, you can
     disable all Build Tools, Test Tools, Swing, Android, and Plugin Development
     plugins. Depending on what projects you plan to work on, you may be able to
     disable more plugins as well, but these are sufficient for now.
   - No need to install any of the featured plugins.
2. Open up the Configure menu in the bottom right hand corner and click
   "Plugins"
   <img src="/plugins-menu.png" alt="Select plugins" />
3. Select the "Marketplace tab" and search for "Cursive". The first result
   should be what you want.
   <img src="/plugins-repo.png" alt="Search for Cursive" />
   
Congratulations! Now you've installed Cursive.
   
# Project Setup

Project setup includes install Clojure, so no need to do that before continuing on.

1. To create a new project, follow [these
   instructions](https://cursive-ide.com/userguide/first-repl.html) from the the
   Cursive documentation. 
2. Once you've created a project and started a repl, you're ready to go! You can
   type code in the box to the bottom right and press Ctrl+Enter (Cmd+Enter on
   Macs) to evaluate the code and display the result above. 
   <img src="/repl-explanation.png" alt="Repl evaluation box is in
   the bottom right of the IntelliJ window." />
   
If you have any questions feel free to ask a staff member in the AppLab or in
the Clojure channel of the [Slack](https://join.slack.com/t/unc-app-lab/shared_invite/enQtNDMyMDQ4NjI1MTg1LTExYjJjNTgzNTA4MzFlMzk5NmU3ZWFmMWQyYTA0MjY5NWY0OGQ1OTlhMzk4NGM1ZDNkNmRhMGE1YTlhZDJjODA).
