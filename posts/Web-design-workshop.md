---
title: Web design UI workshop
author: Lia Abed
date: 2022-02-09
tags: tutorial, introduction, howto, debugging
---

web design can be very overwhelming and confusing if not approached in the right manners, and this blog is about guiding you throughout your journey to build a very beautiful seamless web design using basic html and css code!

<!--more-->

if you are interested in web design and UI this blog is for you!

for starters, one needs to understand that a website is a peice of art and just like a painting you need a sketch to emphasize your ideas and envision your final product.
and for your website you need to sketch your ideas out before you start, to make sure you got every aspect figured out before you jump into coding, so its important to have a wireframe, a design, and a mockup.

### Wireframe!

what is wireframe?
wireframe == your sketch for your website interface, it can be something basic like a scribble on a napkin or something very fancy like a sketch on adobe illustrator.
but for now we will use something that falls in between, a note taking app on my ipad!
but before i even sketch the website i went ahead and designed a site map, like how would my website work cohesively and in a collective manners, what link would take me where and how would it be structured.

<img src="/resources/sitemap.png" width="714" height="216" alt="site map" />

you can take this step a little bit further and also sketch how your website would look on a mobile point of view to also be ready to code it!

### Designing

after your sketch is ready its time to design it into a mockup prototype
for this step i prefer using Figma because it got some special tools where you can create a clickable prototype which can really show you what your code could end up like so make sure you are satisfied with the design before you move forward because while coding you need to go back and refer to the information you have on the Figma design such as margining, buttons functions, padding, colors and so on.
after u are done with the design its very useful to also use the prototype tool and link in your pages to show what each element does in your website and how to navigate throughout your website.

### Coding

Okay now comes the juicy part 🥸, coding..
for web dev I chose to only execute my design in CSS and HTML, to make it quick and easy
if you want to level up your website learning javascript and animating your website would be the way,
but to make it short and sweet i only used Basic HTMl code and for styling i used css code with help of tailwinds libraries to spice up the website and make it look pretty!

so for starters i put all of my html elements in separate dividers and label them by a good name to refer back to later and then start styling them up in my css style sheet or inside of the html code using the documentations from tailwinds!

#### HTML CODE

1. so first for the header i just added the app lab logo and gave it a bit of a margin, centered it in the middle and made it have rounded edges.
2. then created the navigation bar and added the second app lab logo and my nav bar was made with unordered lists flexed horizontally
3. added a new section to put more about the website called recent events and then added a picture
4. the footer had the full logo and a connect with us links.
5. the for the copy rights section i added a different background color and centered my text and colored it white.

#### STYLING CODE

I used some gradient affect from tailwind to create a gradient color affect to my text to give it a subtle cool look.
I changed the hover and active code to give it some animation when the use r hover over the navigation bar and the learn more bar as well.
i made the navigation bar stick to the top of the website to make it more interesting and full.

### Useful Links

[click here](https://v2.tailwindcss.com/) for the tailwinds website

[click here](https://github.com/Lamaab02/workshop-test1/tree/main/workshop) to view the source code

[click here](https://lamaab02.github.io/workshop-test1/) to view the final product

[click here](https://www.canva.com/design/DAE3PaBIXS0/5jl-38hFuYspFuEdNju_yA/view?utm_content=DAE3PaBIXS0&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink) to view the slideshow
