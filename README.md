# UNC App Lab Website

Content for the UNC App Lab web site.

## Prerequisites

To edit the App Lab website content, the only prereq is git. When you push a
commit to master, it will be automatically deployed to the live site hosted by
Netlify.

In order to view changes locally, install the 
[Clojure Cli](https://clojure.org/guides/getting_started) and
[pandoc](https://pandoc.org/installing.html).

## Usage

### Build the site

If you want to preview the changes you make before deploying them, you'll need
to build the site locally. To do that, run this command:

```
clojure -M:ssg
```

This takes about 2 seconds, and stores the built static site in the `dist/`
directory. This directory is git-ignored and will not be included in the
commit. But Netlify will do its own build when it notices a new commit.

The built site in the `dist/` directory has the following structure:

- `/blog/index.html` is a page containing all post summaries, latest first. At
  this point, no pagination is performed.
- Each page is compiled and becomes a top-level directory in the site. For
  example, a page in `pages/about.md` produces `dist/about/index.html`.
- Each post is compiled and becomes a post in a date-based subdirectory under
  `posts/`. For example, a post with certain metadata will produce
  `dist/2018/12/10/slugified-title/index.html`.
- `/authors/index.html` is a page containing a list of post titles for each
  post author.
- `/author/*/index.html` are pages for each author containing post summaries.
- `/tags/index.html` is a page containing a list of post titles for each tag.
  (Note that a single post can appear multiple times, since a post can have
  multiple tags.)
- `/tag/*/index.html` are pages for each tag containing post summaries.

### Add content

There are several sources of content:

- The header and footer templates in the `template/` directory. These are used
  verbatim on every page, except that the title of each page is substituted into
  the header template.
- The site style rules in `resources/style.css`.
- The script and style files for the client-side syntax highlighter in the
  `resources/` directory. If you add other files to this directory, they will
  not be copied automatically (although that wouldn't be a bad feature to add).
- The pages in the `pages/` directory. These are markdown files with YAML
  metadata (which is required). The metadata must include a `title` property.
  Note that the title will be included in the output automatically, so don't
  re-state the title in the markdown.
- The posts in the `posts/` directory. These are like pages, but with extra
  metadata required for the author, date, and tags.

To add a post (or page), I recommend copying an existing post (or page) to get
the metadata formatting right, then replace the metadata and content as
necessary.

### Adding Redirects

Redirects can be added to the website by editing the `resources/redirects` file.
The file consists of a source URL and then a target url separated by whitespace.
It supports both local and absolute URLs for the target URL. See the [netlify
documentation on redirects](https://docs.netlify.com/routing/redirects/) for
more info.

### Develop Locally

You can [run a local web
server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server)
to see everything in your browser.

In order to run the local web server and render the website's content, navigate
to the `dist/` directory, then follow the instructions at the link above or as
shown below.

If using Python 3:

```
python -m http.server
```

If using Python 2:

```
python -m SimpleHTTPServer
```

Your website should be available at `http://localhost:8000`.

## License

Copyright © 2020 UNC App Lab

Distributed under the MIT License.
